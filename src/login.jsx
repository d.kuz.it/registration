import  './App.css';
import {useEffect, useState} from "react";


export const Login = (props) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)
    const [emailError, setEmailError] = useState("имейл не может быть пустым")
    const [passwordError, setPasswordError] = useState('пароль не может быть пустым')
    const [firstName, setFirstName] = useState('')
    const [secondName, setSecondName] = useState('')
    const [firstNameDirty, setFirstNameDirty] = useState(false)
    const [secondNameDirty, setSecondNameDirty] = useState(false)
    const [firstNameError, setFirstNameError] = useState('firstName не может быть пустым')
    const [secondNameError, setSecondNameError] = useState('secondName не может быть пустым')
    const [validButton, setValidButton] = useState(false)


    useEffect(() => {
        if (emailError || passwordError || firstNameError || secondNameError) {
            setValidButton(false)
        } else {
        setValidButton(true)
        }
        },[emailError,passwordError,firstNameError,secondNameError]
    )

    const handlerEmail = (el) => {
        setEmail(el.target.value)
        const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])$/;

        if (!re.test(String(email).toLowerCase())) {
            console.log(re)
            setEmailError('Неккоректный имейл')
        } else {
            setEmailError('')
        }
    }

    const handlerPassword = (el) => {
        setPassword(el.target.value)
        const re = /^(?=.*[0-9])[a-zA-Z0-9]{6,16}$/;
        if (!re.test(String(password))){
            setPasswordError('Пароль должен быть другим ')
            if (!el.target.value) {
            setPasswordError('Пароль не может быть пустым')}
        } else {
            setPasswordError('')
    }}


    const handlerFirstName = (el) => {
        setFirstName(el.target.value)
        const re = /^[a-zA-Z]{2,16}$/
        if (!re.test(String(firstName))){
            setFirstNameError('FirstName должен быть другим ')
            if (!el.target.value) {
                setFirstNameError('FirstName не может быть пустым')}
        } else {
            setFirstNameError('')
        }}


    const handlerSecondName = (el) => {
        setSecondName(el.target.value)
        const re = /^[a-zA-Z]{2,16}$/

        if (!re.test(String(secondName))){
            setSecondNameError('SecondName должен быть другим ')
            if (!el.target.value) {
                setSecondNameError('SecondName не может быть пустым')}
        } else {
            setSecondNameError('')
        }}



    const blurOn = (el) => {
        switch (el.target.name) {
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
            case 'firstName':
                setFirstNameDirty(true)
                break
            case 'secondName':
                setSecondNameDirty(true)
                break
        }

}


    return (
        <div className='authorization'>
            <form className='form'>
                <h1>Login</h1>



                {(emailDirty && emailError) && <div style={{color:'red'}}>{emailError}</div>}
                <input  className='input_container' onChange={el => handlerEmail(el)} value={email} onBlur={el => blurOn(el)} name='email' type='text' placeholder='Enter your email....'/>


                {(passwordDirty && passwordError) && <div style={{color:'red'}}>{passwordError}</div>}
                <br></br>
                <input className='input_container' onChange={el => handlerPassword(el)} value={password} onBlur={el => blurOn(el)} name='password' type='password' placeholder='Enter your password....'/>


               <br></br>
                   <input onChange={el => handlerFirstName(el)} onBlur={el => blurOn(el)} value={firstName} className='input_container'  name='firstName' type='text' placeholder='Enter your firstName....'/>
                {(firstNameDirty && firstNameError) && <div style={{color:'red'}}>{firstNameError}</div>}


                <br></br>
                <input   onChange={el => handlerSecondName(el)}  onBlur={el => blurOn(el)} value={secondName} className='input_container'  name='secondName' type='text' placeholder='Enter your secondName....'/>
                {(secondNameDirty && secondNameError) && <div style={{color:'red'}}>{secondNameError}</div>}


                <br></br>
                <button disabled={!validButton} type="submit" className='button'>Registration</button>
            </form>
        </div>
    )
}